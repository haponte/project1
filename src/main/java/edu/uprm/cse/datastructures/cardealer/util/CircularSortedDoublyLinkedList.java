package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>  {


    private static class Node<E>{
        private E element;
        private Node<E> next;
        private Node<E> previous;

        public Node(E element, Node<E> next, Node<E> previous) {
            super();
            this.element = element;
            this.next = next;
            this.previous = previous;
        }
        public Node() {
            super();
        }

        //Getters
        public E getElement() {
            return element;
        }
        public Node<E> getNext() {
            return next;
        }
        public Node<E> getPrevious(){
            return previous;
        }
        //Setters
        public void setElement(E element) {
            this.element = element;
        }
        public void setNext(Node<E> next) {
            this.next = next;
        }
        public void setPrevious(Node<E> previous) {
            this.previous = previous;
        }

    }

    private class DoublyLinkedListIterator<E> implements Iterator<E> {
        private Node<E> nextNode;


        public DoublyLinkedListIterator() {
            this.nextNode = (Node<E>) header.getNext();
        }

        @Override
        public boolean hasNext() {
            return nextNode != null;
        }

        @Override
        public E next() {
            if (this.hasNext()) {
                E result = this.nextNode.getElement();
                this.nextNode = this.nextNode.getNext();
                return result;
            } else {
                throw new NoSuchElementException();
            }
        }
    }

    private Node<E> header;
    private int currentSize;
    private Comparator<E> comparator;


    public CircularSortedDoublyLinkedList(Comparator C){
        this.header = new Node<E>();
        this.currentSize = 0;
        this.comparator = C;
        this.header.setNext(this.header);
        this.header.setPrevious(this.header);

    }

    @Override
    public boolean add(E obj) {

        if(this.isEmpty()){
            Node<E> temp = new Node<E>(obj,this.header,this.header);
            this.header.setNext(temp); this.header.setPrevious(temp);
            this.currentSize++;

        }
        else{
            Node<E> temp = this.header;
            while (temp.getNext()!= header && comparator.compare(temp.getNext().getElement(),obj) < 0){
                temp = temp.getNext();
            }

            Node<E> newOne = new Node<E>(obj,temp.getNext(),temp);
            temp.setNext(newOne);
            this.currentSize++;
        }



        return true;
    }

    @Override
    public int size() {
        return this.currentSize;
    }

    @Override
    public boolean remove(E obj)
    {
        Node<E> temp = this.header.getNext();
        while(temp!=header){
            if(temp.getElement().equals(obj)){
                Node<E> nextNode = temp.getNext();
                Node<E> prevNode = temp.getPrevious();
                prevNode.setNext(nextNode);
                nextNode.setPrevious(prevNode);
                temp.setElement(null);
                this.currentSize--;
                return true;
            }
            temp = temp.getNext();
        }

        return false;
    }

    @Override
    public boolean remove(int index) {
        if((index <  0 ) || index >= this.size()){
            throw new IndexOutOfBoundsException();
        }
        Node<E> temp = this.getPos(index);
        temp.getPrevious().setNext(temp.getNext());
        temp.getNext().setPrevious(temp.getPrevious());
        temp = null;
        this.currentSize--;
        return true;
    }

    @Override
    public int removeAll(E obj) {

        int count = 0;
        while (remove(obj)){
            count++;
        }
        return count;
    }

    @Override
    public E first() {
        return this.header.getNext().getElement();
    }

    @Override
    public E last() {
        return this.get(this.currentSize-1);
    }

    @Override
    public E get(int index) {
        if((index <  0 ) || index >= this.size()){
            throw new IndexOutOfBoundsException();
        }
        Node<E> temp = this.getPos(index);

        return temp.getElement();

    }

    @Override
    public void clear() {

        while(!isEmpty()){
            this.remove(0);
        }

    }

    @Override
    public boolean contains(E o) {
        Node<E> temp = this.header.getNext();
        while(temp !=this.header){
            if(temp.getElement().equals(o)){
                return true;
            }
            temp = temp.getNext();
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public int firstIndex(E o) {
        Node<E> temp = this.header.getNext();
        int index = 0;
        while(temp !=this.header){
            if(temp.getElement().equals(o)){
                return index;
            }
            index++;
            temp = temp.getNext();
        }
        return -1;
    }

    @Override
    public int lastIndex(E o) {
        int index = this.size() - 1;
        Node<E> temp = this.getPos(index);
        while(temp != this.header){
            if(temp.getElement().equals(o)){
                return index;
            }
            index--;
            temp = temp.getPrevious();
        }
        return -1;
    }

    @Override
    public Iterator iterator() {
        return new DoublyLinkedListIterator<E>();
    }

    //HELPER METHOD

    private Node<E> getPos(int pos) {
       int currentPos = 0;
       Node<E> temp = this.header.getNext();
       while(currentPos != pos){
           temp = temp.getNext();
           currentPos++;
       }
        return temp;

    }
}
